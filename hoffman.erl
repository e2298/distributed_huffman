-module(hoffman).
-export([compress/2, decompress/2, do_server/4, do_client/2]).

chunk_size() -> 256.

do_server(IFilename, OFilename, Port, Clients) ->
	{_, LSock} = gen_tcp:listen(Port, [binary, {packet, 4}, {active, false}]),
	{_, IFile} = file:open(IFilename, [read, binary, raw]),
	{_, OFile} = file:open(OFilename, [write, binary, raw]),
	FSize = filelib:file_size(IFilename),
	file:write(OFile, <<FSize:64>>),
	Socks = setup_clients(LSock, IFile, FSize, Clients, 0),
	OccurrencesT = lists:foldl(fun(Sock, Acc) -> 
		{_, OccData} = gen_tcp:recv(Sock, 0),
		dict_from_bin(OccData, 0, Acc) end,
	  dict:new(), Socks),
	write_occurrences(0, OccurrencesT, OFile),
	OccurrencesBin = list_to_binary([<<X:64>> || {_,X} <- lists:sort(fun({<<A:8>>,_}, {<<B:8>>,_}) -> A =< B end, dict:to_list(OccurrencesT))]),
	lists:foreach(fun(Sock) -> gen_tcp:send(Sock, OccurrencesBin) end, Socks),
	Leftover = lists:foldl(fun(Sock, Acc) -> 
		{_, <<Sz:64>>} = gen_tcp:recv(Sock, 0),
		recieve_write(Sock, OFile, Acc, Sz) end,
	  <<>>, Socks),
	file:write(OFile, make_binary(Leftover)).

recieve_write(_, _, Leftover, 0) -> Leftover;
recieve_write(Sock, File, Leftover, Size) ->
	{_, Data} = gen_tcp:recv(Sock, 0),
	gen_tcp:send(Sock, <<0:64>>),
	DataSize = min(Size, chunk_size() * 8),
	<<NData:DataSize/bitstring, _/bitstring>> = Data,
	Joined = <<Leftover/bitstring, NData/bitstring>>,
	Write_size = bit_size(Joined) div 8,
	<<To_write:Write_size/binary, Rest/bitstring>> = Joined,
	file:write(File, To_write),
	recieve_write(Sock, File, Rest, Size - DataSize).


setup_clients(_, _, _, ClientsT, ClientsT) -> [];
setup_clients(LSock, IFile, FSize, ClientsT, Client) -> 
	{_, NSock} = gen_tcp:accept(LSock),
	TSize = FSize div ClientsT + if Client < (FSize rem ClientsT) -> 1; true -> 0 end,
	gen_tcp:send(NSock, <<TSize:64>>),
	transfer(NSock, IFile, TSize),
	[NSock| setup_clients(LSock, IFile, FSize, ClientsT, Client+1)].

transfer(_, _, 0) -> ok;
transfer(Socket, File, Size) -> 
	NSize = min(Size, chunk_size()),
	{_, Data} = file:read(File, NSize),
	gen_tcp:send(Socket, Data),
	gen_tcp:recv(Socket, 0), %wait for acknowledgement so buffer doesnt fill up
	transfer(Socket, File, Size - NSize).

do_client(Port, Hostname) -> 
	{_, SSock} = gen_tcp:connect(Hostname, Port, [binary, {packet, 4}, {active, false}]),      
	{_, TFile} = file:open("tmpin", [write, binary, raw]),
	{_, <<Size:64>>} = gen_tcp:recv(SSock, 0),
	recieve(SSock, TFile, Size),
	file:close(TFile),
	{_, IFile} = file:open("tmpin", [read, binary, raw]),
	{_, OFile} = file:open("tmpout", [write, binary, raw]),
	OccurrencesT = count_chunks(IFile, dict:from_list( [{<<X:8>>,0} || X <- lists:seq(0,255)])),
	gen_tcp:send(SSock, list_to_binary([<<X:64>> || {_,X} <- lists:sort(fun({<<A:8>>,_}, {<<B:8>>,_}) -> A =< B end, dict:to_list(OccurrencesT))])),
	{_, OccData} = gen_tcp:recv(SSock, 0),
	Occurrences = dict_from_bin(OccData, 0, dict:new()),
	Codes = get_codes(build_tree(dict:filter(fun(_,Val) -> Val /= 0 end, Occurrences))),
	file:position(IFile, 0),
	Bit_size = compress_chunks(IFile, OFile, Codes, <<>>),
	gen_tcp:send(SSock, <<Bit_size:64>>),
	file:close(OFile),
	{_, DFile} = file:open("tmpout", [read, binary, raw]),
	transfer(SSock, DFile, (Bit_size + 7) div 8).

dict_from_bin(<<>>, _, Dict) -> Dict;
dict_from_bin(<<Curr:64, Rest/binary>>, N, Dict) -> dict_from_bin(Rest, N+1, dict:update_counter(<<N:8>>, Curr, Dict)).

recieve(_, _, 0) -> ok;
recieve(Socket, File, Size) ->
	NSize = min(Size, chunk_size()), 
	{_, Data} = gen_tcp:recv(Socket, 0),
	gen_tcp:send(Socket, <<0:64>>),
	file:write(File, Data),
	recieve(Socket, File, Size - NSize).

compress(Filename, OFilename) -> 
	%{_, Bin} = file:read_file(Filename).
	{_,File} = file:open(Filename, [read, binary, raw]),
	{_,Ofile} = file:open(OFilename, [write, binary, raw]),
	Len = filelib:file_size(Filename),
	file:write(Ofile, <<Len:64>>),
	OccurrencesT = count_chunks(File, dict:from_list( [{<<X:8>>,0} || X <- lists:seq(0,255)])),
	write_occurrences(0, OccurrencesT, Ofile),
	Occurrences = dict:filter(fun(_,Val) -> Val /= 0 end, OccurrencesT),
	Codes = get_codes(build_tree(Occurrences)),
	file:position(File, 0),
	compress_chunks(File, Ofile, Codes, <<>>).

compress_chunks(File, Ofile, Codes, Acc) ->
	Res = file:read(File,chunk_size()),
	  if Res == eof -> 
		file:write(Ofile, make_binary(Acc)),
	       	0;	
	     true  ->
		{_,Bin} = Res,
		{Sz, Temp} = do_compress(Bin,Codes),
		Comp = <<Acc/bitstring, Temp/bitstring>>,
		Write_size = (bit_size(Comp) div 8),
		<<To_write:Write_size/binary, Rest/bitstring>> = Comp,
		file:write(Ofile, To_write),
		Sz + compress_chunks(File, Ofile, Codes, Rest)
	  end.

do_compress(<<>>, _) -> {0, <<>>};
do_compress(<<A:8, B/bitstring>>, Codes) -> 
	Code = dict:fetch(<<A:8>>, Codes), 
	{Sz, Rest} = do_compress(B, Codes),
	{bit_size(Code) + Sz, <<Code/bitstring, Rest/bitstring>>}.

decompress(Filename, OFilename) ->
	{_,File} = file:open(Filename, [read, binary, raw]),
	{_,Ofile} = file:open(OFilename, [write, binary, raw]),
	{_, <<Len:64>>} = file:read(File, 8),
	Occ = dict:filter(fun(_,Val) -> Val /= 0 end, read_occurrences(file:read(File, 256*8))),
	Tree = build_tree(Occ),
	decompress(File, Ofile, Tree, Len).
decompress(File, Ofile, Tree, Size) -> decompress(File, Ofile, <<>>, Tree, Tree, Size).
decompress(_, _, _, _, _, 0) -> ok;
decompress(_, Ofile, _, {<<Char/binary>>, _}, T, Sz) -> file:write(Ofile, Char), decompress(ok, Ofile, <<>>, T, T, Sz-1);
decompress(File, Ofile, <<>>, TRoot, TCurr, Sz) -> {_, Data} = file:read(File, chunk_size()), decompress(File, Ofile, Data, TRoot, TCurr, Sz);
decompress(File, Ofile, <<0:1, Rest/bitstring>>, TRoot, {{tree, Left, _},_}, Sz) -> decompress(File, Ofile, Rest, TRoot, Left, Sz);
decompress(File, Ofile, <<1:1, Rest/bitstring>>, TRoot, {{tree, _, Right},_}, Sz) -> decompress(File, Ofile, Rest, TRoot, Right, Sz);
decompress(File, Ofile, Data, TRoot, {<<Char/binary>>, _}, Sz) -> file:write(Ofile, Char), decompress(File, Ofile, Data, TRoot, TRoot, Sz-1).


read_occurrences({_, Bin}) -> read_occurrences(0, Bin, dict:new()).
read_occurrences(256, _, Acc) -> Acc;
read_occurrences(N, <<Times:64, Rest/binary>>, Acc) -> read_occurrences(N+1, Rest, dict:store(<<N:8>>, Times, Acc)).

write_occurrences(256, _, _) -> true;
write_occurrences(N, Occ, Ofile) -> 
	To_write = dict:fetch(<<N:8>>, Occ),
	file:write(Ofile, <<To_write:64>>), write_occurrences(N+1, Occ, Ofile).

get_codes(Tree) -> get_codes(Tree, dict:new(), 0, 0).
get_codes({{tree,Left,Right},_}, Dict, Code, Code_len) -> 
	Dict1 = get_codes(Left, Dict, Code*2, Code_len + 1),
	get_codes(Right, Dict1, Code*2+1, Code_len + 1);
get_codes({Char, _}, Dict, Code, Code_len) -> 
	dict:store(Char, <<Code:Code_len>>, Dict).

%leaves: {Byte/bitstring, Size}
%other nodes: {{tree, Left, Right}, Size}
build_tree(Occurrences) -> 
	Q1 = queue:from_list(lists:sort(fun({X,A}, {Y,B}) -> if A == B -> X =< Y; true -> A =< B end end, dict:to_list(Occurrences))),
	Q2 = queue:new(),
	build_tree(Q1, Q2, queue:len(Q1), 0).
build_tree(Q1, Q2, S1, S2) -> 
	if S1 == 0 , S2 == 0 -> empty;
	   S1 == 0 , S2 == 1 -> queue:get(Q2);
	   S2 == 0 , S1 == 1 -> queue:get(Q1);
	   true -> {Q1a, Q2a, S1a, S2a} = join(Q1, Q2), build_tree(Q1a, Q2a, S1-S1a, S2-S2a + 1)
	end.

%true if T1 comes before T2 
compare_trees(empty, _) -> false;
compare_trees(_, empty) -> true;
compare_trees({_, {_,S1}}, {_, {_,S2}}) -> S1 < S2.


%joins 2 lowest elements 
join(Q1, Q2) ->
	Comp1 = compare_trees(queue:peek(Q1), queue:peek(Q2)),
	{Q1a, Q2a, S1a, S2a, E1} = if Comp1 == true -> {queue:drop(Q1), Q2, 1, 0, queue:get(Q1)};
				      true -> {Q1, queue:drop(Q2), 0, 1, queue:get(Q2)}
				   end,
	Comp2 = compare_trees(queue:peek(Q1a), queue:peek(Q2a)),
	{Q1b, Q2b, S1b, S2b, E2} = if Comp2 == true -> {queue:drop(Q1a), Q2a, S1a+1, S2a, queue:get(Q1a)};
				      true -> {Q1a, queue:drop(Q2a), S1a, S2a+1, queue:get(Q2a)}
				   end,
	{_,SE1} = E1,
	{_,SE2} = E2,
	{Q1b, queue:in({{tree,E1,E2}, SE1+SE2}, Q2b), S1b, S2b}.


make_binary(<<Binary/binary>>) -> Binary;
make_binary(<<Bitstring/bitstring>>) -> make_binary(<<Bitstring/bitstring, 0:1>>).

count_chunks(File, Acc) ->
	Res = file:read(File,chunk_size()),
	  if Res == eof -> Acc; 
	     true  ->
		{_,Bin} = Res,
		count_chunks(File, count(Bin, Acc))
	  end.

%counts occurrences of bytes
count(<<>>, Counter) -> Counter;
count(<<A:8, B/binary>>, Counter) -> count(B, dict:update_counter(<<A:8>>, 1, Counter)).
%count(A, B) -> count(byte_size(A), A, B).
%count(0, _, Counter) -> Counter;
%count(N, Bin, Counter) -> 
%	S = N-1,
%	<<Rest:S/binary, Curr:8>> = Bin,
%	count(N-1, Rest, dict:update_counter(<<Curr:8>>, 1, Counter)).

